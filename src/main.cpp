#include <iostream>
#include "Graph.h"
#include "Bar_Plot.h"
#include "Line_Plot.h"
#include "Histogram_Plot.h"
#include "Circual_Plot.h"
#include <fstream>
#include <vector>


using namespace std;


/**
 * @mainpage Graphify
 *
 * This program is meant to draw in ASCII characters a plot based on input file
 *
 * All example input files are in examples/ folder.
 * Usage: ./trnkadus ./examples/config ./examples/*any input file from examples*
 *
 * Config file format:
 *
 *  <int>,<int>,*any number of individual char separated by ,*
 *
 *  First int means height of desired graph.
 *  Secound int is only used by Histrogram Plot and it is used for setting how many charaters wide should one column be.
 *  Other characters represents the filling of graph. For pie chart for example.
 *
 *
 *  Example usage:
 *
 *
 * ./trnkadus ./examples/config ./examples/sinus
 * ./trnkadus ./examples/config ./examples/cosinus
 *
 * ./trnkadus ./examples/wideconfig ./examples/sinus and choose histogram.
 * ./trnkadus ./examples/config ./examples/randominput1 and choose pie chart.
 *
 *
 *
 */


bool PlotProcess(Plot *AnyPlot, char* InputFile, size_t Height = 21, size_t Width = 5, vector<char> Character = {'#'})
{


    if(!AnyPlot->Get_Data( InputFile )) {cout << "oops!" << endl << "Something went wrong by reading from file\n Usage: trnkadus <config> <source file> \n" << endl;
        return false;}

    if(!AnyPlot->Compute( Height )){cout << "oops!" << endl << "Something went wrong by computing" << endl;
        return false;}

    if(!AnyPlot->Render_Plot_To_Buffer ( Character )){cout << "oops!" << endl << "Something went wrong by rendering" << endl;
    return false;};

    AnyPlot->Output_Plot_to_File();


    return true;
}

bool LinePlotCombined(char* Name1, char* Name2 , size_t Height, vector<char> Char_List ){

    Line_Plot  FirstPlot;
    Line_Plot  SecondPlot;


  SecondPlot.Get_Data( Name2 );
    if(!SecondPlot.Compute( Height )){cout << "oops!" << endl << "Something went wrong by computing" << endl;
        return false;}

    FirstPlot.Get_Data( Name1 );
    if(!FirstPlot.Compute( Height )){cout << "oops!" << endl << "Something went wrong by computing" << endl;
        return false;}

    if(FirstPlot.ReturnValueForChar() > SecondPlot.ReturnValueForChar()){
        SecondPlot.SetValueForChar(FirstPlot.ReturnValueForChar());
    }else{
        FirstPlot.SetValueForChar(SecondPlot.ReturnValueForChar());
    }

    if(FirstPlot.ReturnNegativity() || SecondPlot.ReturnNegativity()){
        FirstPlot.SetNegativity(true);
        SecondPlot.SetNegativity(true);
    }




    FirstPlot.Render_Plot_To_Buffer( Char_List );
    Char_List.erase(Char_List.begin());
    SecondPlot.Render_Plot_To_Buffer( Char_List);

    if(FirstPlot.GetLenght() > SecondPlot.GetLenght() ){

        FirstPlot.Combined_Output_Plot_to_File("Output", SecondPlot);
    }else{
        SecondPlot.Combined_Output_Plot_to_File("Output", FirstPlot);
    }



    return  true;
}

bool Histogram_Bar_Combined(char* Name1, char* Name2 , size_t Height, size_t Width, vector<char> Char_List ){

    Histogram_Plot  FirstPlot;
    Histogram_Plot  SecondPlot;


    SecondPlot.Get_Data( Name2 );
    if(!SecondPlot.Compute( Height )){cout << "oops!" << endl << "Something went wrong by computing" << endl;
        return false;}

    FirstPlot.Get_Data( Name1 );
    if(!FirstPlot.Compute( Height )){cout << "oops!" << endl << "Something went wrong by computing" << endl;
        return false;}

    if(FirstPlot.ReturnValueForChar() > SecondPlot.ReturnValueForChar()){
        SecondPlot.SetValueForChar(FirstPlot.ReturnValueForChar());
    }else{
        FirstPlot.SetValueForChar(SecondPlot.ReturnValueForChar());
    }

    if(FirstPlot.ReturnNegativity() || SecondPlot.ReturnNegativity()){
        FirstPlot.SetNegativity(true);
        SecondPlot.SetNegativity(true);
    }

    FirstPlot.Set_Bar_Width(Width);
    SecondPlot.Set_Bar_Width(Width/2);


    FirstPlot.Render_Plot_To_Buffer( Char_List );
    Char_List.erase(Char_List.begin());
    SecondPlot.Render_Plot_To_Buffer( Char_List);

    if(FirstPlot.GetLenght() > SecondPlot.GetLenght() ){

        FirstPlot.Combined_Output_Plot_to_File("Output", SecondPlot);
    }else{
        SecondPlot.Combined_Output_Plot_to_File("Output", FirstPlot);
    }


    return  true;
}

void CombinedPlot(int argc, char *argv[] , size_t Height, size_t Width, vector<char> Char_List){

    if(argc != 4){cout << "I`m sorry this feature is only implemented for two input files and config "<< endl; return;}

    cout<< "Enter 1 for Combined Line Plot \n"
                   "Enter 2 for Combined Histogram Plot \n" << endl;

    char str;
    cin >> str;

    switch(str){
        case '1': LinePlotCombined(argv[2], argv[3], Height, Char_List ); break;
        case '2' : Histogram_Bar_Combined(argv[2], argv[3], Height, Width, Char_List); break;
        default: break;
    }




}





int main(int argc, char *argv[]) {

    int Height;
    int Width;
    vector<char> Char_List;
    if(argc <= 1) {
        cout << "Error no input file was stated\nUsage: trnkadus <config> <source file> \n " << endl;
        return 0;
    }

    ofstream output( "Output", ios::out);
    output.close();



    if(argc == 2){
        cout << "Enter Number for Plot Type\n"
                "1 for bar plot\n"
                "2 for line plot\n"
                "3 for histogram plot\n"
                "4 for pie chart\n" << endl;
        char str;
        cin >> str;
        Plot *p = NULL;
        switch (str){
            case '1' : p = new Bar_Plot ; break;
            case '2' : p = new Line_Plot; break;
            case '3' : p = new Histogram_Plot; break;
            case '4' : p = new Circual_Plot; break;
            default: cout << "Wrong choice of Number " << endl; return false;

        }

        if(!PlotProcess(p, argv[1])){delete p; return false;}
        delete p;
        return 0;
    }

    ifstream inputFile( argv[1], ios::in );
    if( inputFile.is_open() ){
        string line;
        getline ( inputFile , line , ',');
        Height = stoul(line);
        getline ( inputFile , line , ',');
        Width = stoul(line);
        while( getline ( inputFile , line , ',') ){

            Char_List.push_back(line[0]);

        }
    }
    else{
        cout << "Config file was not loaded\nUsage: trnkadus <config> <source file> \n" << endl;
        return false;}

    inputFile.close();

    if(Height <= 1 || Width <= 1){cout<< "Wrong height or width entered. Height or Width has to be greater then 1." << endl;
    return false;}

    //if(!inputFile.good())return false;



    cout << "Enter Number for Plot Type\n"
            "1 for bar plot\n"
            "2 for line plot\n"
            "3 for histogram plot\n"
            "4 for pie chart\n"
            "5 for combined plot\n" << endl;

    for (int i = 2; i < argc; ++i) {

        if(i > 2){cout<< "Enter number for " << i-1 << ". plot" << endl;}
        char str;
        cin >> str;
        Plot *p;
        Histogram_Plot *h_p;
        switch (str){
            case '1' : p = new Bar_Plot ; break;
            case '2' : p = new Line_Plot; break;
            case '3' : h_p = new Histogram_Plot; h_p->Set_Bar_Width(Width); p = h_p ; break;
            case '4' : p = new Circual_Plot; break;
            case '5' : CombinedPlot(argc, argv, Height, Width, Char_List); return 0;
            default: cout << "Wrong choice of Number " << endl; return false;

        }

        if(!PlotProcess(p, argv[i], Height, Width, Char_List)){delete p; return false;}
        delete p;
    }



    return 0;


}
