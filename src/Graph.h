//
// Created by sirin on 27.05.16.
//

#ifndef SEMESTRALKA_GRAPH_H
#define SEMESTRALKA_GRAPH_H


#include <string>
#include <vector>



/**
    *  This class is starting point for polymorphism  and core of every plot.
    *
    *
    */
class Plot {
public:

    /**
     * Only empty implicit constructor since we don`t know other parameters yet.
     */
    Plot();
    /**
     * Virtual destructor
     */
    virtual ~Plot();
    /**
     * Method to load data from file and store them in Raw_Data vector.
     * @return TRUE when everything was OK and FALSE when for example file was not found, file wasnt properly closed or oppened
     */
    virtual bool Get_Data( const char* input );
    /**
     * Compute method is making all calculations needed before we start to draw a plot
     *
     * @param[in] Height is user set maximum height of future plot.
     * @return  Returns TRUE when OK and FALSE when there was some miscalculation.
     */
    virtual bool Compute(const size_t Height ) = 0;
   /**
    * This method does all rendering.
    *
    * @param[in] Character there is vector of characters to be used for filling of plot.
    * @return It returns FALSE if there was some illegal action and TRUE when everything was smoothly.
    */
    virtual bool Render_Plot_To_Buffer(std::vector<char>& Character) = 0;
    /**
     * This method is rendering to the output file based on what plot type we have and data in Plot_Buffer.
     *
     * @return TRUE when OK. FALSE when there was problem with the file.
     * @param[in] Name is the name of the output file.
     */
    virtual bool Output_Plot_to_File(const std::string& Name = "Output" ) = 0;

protected:


    /**
     * Vector to store a raw data from file.
     */
   std::vector<std::pair<std::string, double>> Raw_Data;
    /**
     * Buffer to store rendered plot.
     */

    std::vector<std::vector<char>>Plot_Buffer;

};



#endif //SEMESTRALKA_GRAPH_H
