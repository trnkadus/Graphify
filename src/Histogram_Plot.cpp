//
// Created by sirin on 14.06.16.
//

#include "Histogram_Plot.h"
#include <cmath>
#include <fstream>


Histogram_Plot::Histogram_Plot( ) { }

Histogram_Plot::~Histogram_Plot() { }

size_t Histogram_Plot::GetLenght() const {
    return Plot_Lenght;
}

void Histogram_Plot::Render_Matrix_to_Buffer(const size_t Height) {

    CalcLenght();

    Plot_Buffer.resize(Height);
    for (size_t k = 0; k < Height; ++k) {
        Plot_Buffer[k].resize(Plot_Lenght);
    }
    for (size_t k = 0; k < Height; ++k) {
        for (size_t i = 0; i < Plot_Lenght; ++i) {
            Plot_Buffer[k][i] = ' ';
        }
    }


    if (Negativity) {

        for (size_t i = 0; i < Height; ++i) {
            if (i == Height / 2) {
                Plot_Buffer[i][0] = '+';
                for (size_t j = 1; j < Plot_Lenght; ++j) {
                    Plot_Buffer[i][j] = '.';
                }
                continue;
            }
            Plot_Buffer[i][0] = '|';
        }


    } else {
        for (size_t i = 0; i < Height; ++i) {
            if (i == Height - 1) {
                Plot_Buffer[i][0] = '+';
                for (size_t j = 1; j < Plot_Lenght; ++j) {
                    Plot_Buffer[i][j] = '.';
                }
                break;
            }
            Plot_Buffer[i][0] = '|';
        }
    }




}

bool Histogram_Plot::Render_Plot_To_Buffer(vector<char>& Character) {

   //if(HeightConstant < 1){return false;}

    if (Negativity && !(HeightConstant % 2)) {
        size_t ReducedHeight = HeightConstant - 1;
        Render_Matrix_to_Buffer(ReducedHeight);

        for (size_t i = 0; i < ReducedHeight / 2; ++i) {
            for (size_t j = 1, k = 0; j < Plot_Lenght &&     k < X_Axis_Element_Num; k++) {

                if (((Raw_Data[k].second / ValueForCharacter) + 0.50001) > (ReducedHeight / 2 - i)) {

                    for (size_t l = 0; l < Bar_Width; ++l) {
                        Plot_Buffer[i][j++] = Character[0];
                    }
                } else {
                    j = j + Bar_Width;
                }
            }
        }
        for (size_t i = (ReducedHeight / 2) + 1; i < ReducedHeight; ++i) {
            for (size_t j = 1, k = 0; j < Plot_Lenght && k < X_Axis_Element_Num; k++) {

                if (Raw_Data[k].second > 0) {
                    j = j + Bar_Width;
                    continue;
                }
                if (abs(((Raw_Data[k].second / ValueForCharacter)) - 0.50001) > (i - (ReducedHeight / 2))) {

                    for (size_t l = 0; l < Bar_Width; ++l) {
                        Plot_Buffer[i][j++] = Character[0];
                    }
                } else {
                    j = j + Bar_Width;
                }
            }
        }


        return true;
    } else {
        Render_Matrix_to_Buffer(HeightConstant);
        //Since nothing special is needed here we continue method as intended.
    }

    //RENDER THE DATA;

    if (Negativity) {
        for (size_t i = 0; i < HeightConstant / 2; ++i) {

            for (size_t j = 1, k = 0; j < Plot_Lenght  && k < X_Axis_Element_Num;  k++) {

                if (((Raw_Data[k].second / ValueForCharacter) + 0.50001) > (HeightConstant / 2 - i)) {

                    for (size_t l = 0; l < Bar_Width; ++l) {
                        Plot_Buffer[i][j++] = Character[0];
                    }

                } else {
                    j = j + Bar_Width;
                }
            }
        }
        for (size_t i = (HeightConstant / 2) + 1; i < HeightConstant; ++i) {
            for (size_t j = 1, k = 0; j < Plot_Lenght  && k < X_Axis_Element_Num; k++) {

                if (Raw_Data[k].second > 0) {
                    j = j + Bar_Width;
                    continue;
                }
                if (abs(((Raw_Data[k].second / ValueForCharacter)) - 0.50001) > (i - HeightConstant / 2)) {

                    for (size_t l = 0; l < Bar_Width; ++l) {
                        Plot_Buffer[i][j++] = Character[0];
                    }
                } else {
                    j = j + Bar_Width;
                }
            }
        }


    }


    else {
        for (size_t i = 0; i < HeightConstant - 1; ++i) {
            for ( size_t j = 1, k = 0; j < Plot_Lenght && k < X_Axis_Element_Num; k++) {


                if (((Raw_Data[k].second / ValueForCharacter) + 0.50001) > ((HeightConstant - 1) - i)) {

                    for (size_t l = 0; l < Bar_Width; ++l) {
                        Plot_Buffer[i][j++] = Character[0];
                    }
                } else {
                    j = j + Bar_Width;
                }
            }
        }
    }

    return true;
}

bool Histogram_Plot::Set_Bar_Width(const size_t Width) {
    if(Width == 0){return false;}
    Bar_Width = Width;
return true;
}

bool Histogram_Plot::Output_Plot_to_File(const string& Name  ) {

    Render_Legend_To_Buffer();

    ofstream OutFile;

    OutFile.open(Name, ios::out | ios::app);
    if(!OutFile.is_open())return false;

    if (Negativity) {

        if (HeightConstant % 2 == 0) { HeightConstant = HeightConstant - 1; };

        for (size_t i = 0; i < HeightConstant; ++i) {

            if (i < HeightConstant / 2) {
                OutFile << right << setprecision(4) << setw(6) << (((HeightConstant - 1) / 2) - i) * ValueForCharacter;
            }
            else {
                OutFile << right << setprecision(4) << setw(6) <<
                (i - ((HeightConstant - 1) / 2)) * ValueForCharacter * -1;
            }
            for (size_t j = 0; j < Plot_Lenght; ++j) {
                OutFile << Plot_Buffer[i][j] << ' ';
            }
            if (i < Legend_Buffer.size()) {
                OutFile << '\t' << Legend_Buffer[i] << endl;
            } else { OutFile << endl; }

        }
        OutFile.close();
        if(!OutFile.good())return false;
        return true;
    }

    for (size_t i = 0; i < HeightConstant; ++i) {

        OutFile << right << setprecision(4) << setw(6) << ((HeightConstant - 1) - i) * ValueForCharacter;

        for (size_t j = 0; j < Plot_Lenght; ++j) {
            OutFile << Plot_Buffer[i][j] << ' ';
        }
        if (i < Legend_Buffer.size()) {
            OutFile << '\t' << Legend_Buffer[i] << endl;
        } else { OutFile << endl; }
    }

    OutFile.close();
    if( !OutFile.good() )return false;
    return true;
}

bool Histogram_Plot::Combined_Output_Plot_to_File(const string& Name, Histogram_Plot & OtherPlot) {

    Render_Legend_To_Buffer();
    OtherPlot.Render_Legend_To_Buffer();


    ofstream OutFile;

    OutFile.open(Name, ios::out | ios::app);
    if(!OutFile.is_open())return false;

    if (Negativity) {

        if (HeightConstant % 2 == 0) { HeightConstant = HeightConstant - 1; };

        for (size_t i = 0; i < HeightConstant; ++i) {

            if (i < HeightConstant / 2) {
                OutFile << right << setprecision(4) << setw(6) << (((HeightConstant - 1) / 2) - i) * ValueForCharacter;
            }
            else {
                OutFile << right << setprecision(4) << setw(6) <<
                (i - ((HeightConstant - 1) / 2)) * ValueForCharacter * -1;
            }
            for (size_t j = 0; j < Plot_Lenght; ++j) {

                if(j < OtherPlot.Plot_Lenght){

                    if(OtherPlot.Plot_Buffer[i][j] != ' '){

                        OutFile << OtherPlot.Plot_Buffer[i][j] << ' ';

                    }else{
                        OutFile << Plot_Buffer[i][j] << ' ';
                    }
                }
                else{
                    OutFile << Plot_Buffer[i][j] << ' ';
                }
            }

            if (i < Legend_Buffer.size()) {
                OutFile << '\t' << Legend_Buffer[i] ;
                if( i < OtherPlot.Legend_Buffer.size()){ OutFile <<  "|||||  " << OtherPlot.Legend_Buffer[i] << endl;}
            } else { OutFile << endl; }

        }
        OutFile.close();
        if(!OutFile.good())return false;
        return true;
    }

    for (size_t i = 0; i < HeightConstant; ++i) {

        OutFile << right << setprecision(4) << setw(6) << ((HeightConstant - 1) - i) * ValueForCharacter;

        for (size_t j = 0; j < Plot_Lenght; ++j) {

            if(j < OtherPlot.Plot_Lenght){

                if(OtherPlot.Plot_Buffer[i][j] != ' '){


                    OutFile << OtherPlot.Plot_Buffer[i][j] << ' ';


                }else{
                    OutFile << Plot_Buffer[i][j] << ' ';
                }
            }
            else{
                OutFile << Plot_Buffer[i][j] << ' ';
            }
        }
        if (i < Legend_Buffer.size()) {
            OutFile << '\t' << Legend_Buffer[i] ;
            if( i < OtherPlot.Legend_Buffer.size()){ OutFile <<  "|||||  " <<  OtherPlot.Legend_Buffer[i] << endl;}
        } else { OutFile << endl; }
    }

    OutFile.close();
    if( !OutFile.good() )return false;
    return true;
}
