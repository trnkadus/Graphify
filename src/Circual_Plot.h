//
// Created by sirin on 13.06.16.
//

#ifndef SEMESTRALKA_CIRCUAL_PLOT_H
#define SEMESTRALKA_CIRCUAL_PLOT_H

#include "Graph.h"
#include <vector>

/**
 * This class is build to address all needs of pie chart.
 * It makes all calculation here and rendering.
 *
 */
class Circual_Plot : public Plot{
public:

    /**
     * Default constructor.
     */
    Circual_Plot();
/**
 * Default destructor.
 */
    virtual ~Circual_Plot();
    /**
      * Compute method is making all calculations needed before we start to draw a plot
      *
      * @param[in] size is user set maximum height of future plot.
      * @return  Returns TRUE when OK and FALSE when there was some miscalculation.
      */
    virtual bool Compute(const size_t size);
    /**
   * This method does all rendering.
   *
   * @param[in] Character there is vector of characters to be used for filling of plot.
   * @return It returns FALSE if there was some illegal action and TRUE when everything was smoothly.
   */
    virtual bool Render_Plot_To_Buffer(std::vector<char>& Character) ;
/**
 * This method choses which character should go on witch angle.
 *
 * @param[in] Angle it is the angle of Y/X to the center of pie chart.
 * @return It returns character which should be on its angle in pie chart.
 */
    char ReturnCharacter(const double Angle);
/**
        * This method is rendering to the output file based on what plot type we have and data in Plot_Buffer.
        *
        * It alse renders the Legend right next to buffer.
        *
        * @return TRUE when OK. FALSE when there was problem with the file.
        * @param[in] Name is the name of the output file.
        */
    virtual bool Output_Plot_to_File(const std::string& Name);


protected:
/**
 * It is the radius of pie chart
 *
 * Default is 31.
 */
    size_t Radius = 31;
    /**
     * It is number of elements we are going to have in pie chart.
     */
    size_t NumberOfElements;
    /**
     * Struct used to hold up all the information we need to render Pie Chart
     */
    struct OnePieceOfPie{
/**
 * Constructor of Struct
 */
        OnePieceOfPie(std::  string Name, double Value, double Percentage, double Degree){

            S_Name = Name;
            S_Value = Value;
            S_Percentage = Percentage;
            S_Degree = Degree;

        }

        /**
         * Name of the pie
         */
        std::string S_Name;
        /**
         * Integer values of pie
         */
        double S_Value;
        /**
         * Percentage value of pie.
         */
        double S_Percentage;
         /**
          * Degree value of pie.
          */
        double S_Degree;

    };

    /**
     * Vector which holds up all the characters we can use for pie chart rendering.
     */
    std::vector<char> List_Char = {'$', '%', '#' , '&', '@' , '&' , '?' , '~', '^' , 'X' , '=' , '+' , 'P'};
    /**
     * Vector of OnePiecePie struct to hold up all information we need about pie chart pies.
     */
   std::vector<OnePieceOfPie> PiePiece;



};


#endif //SEMESTRALKA_CIRCUAL_PLOT_H
