//
// Created by sirin on 04.06.16.
//

#ifndef SEMESTRALKA_HORIZONTAL_PLOT_H
#define SEMESTRALKA_HORIZONTAL_PLOT_H

#include "Graph.h"
#include <iomanip>
#include <iostream>

    /**
      * This class is parent of all horizontal plots. Is intented to present polyporphism at it`s brightest form.
      */
class Horizontal_Plot : public Plot {
public:

    /**
     * Default constructor
     */
    Horizontal_Plot();
    /**
     * Default destructor
     */
    virtual ~Horizontal_Plot();
/**
        * Compute method is making all calculations needed before we start to draw a plot
        *
        * @param[in] Height is user set maximum height of future plot.
        * @return  Returns TRUE when OK and FALSE when there was some miscalculation.
        */
    virtual bool Compute(const size_t Height);
    /**
    * This method does all rendering.
    *
    * @param[in] Character there is vector of characters to be used for filling of plot.
    * @return It returns FALSE if there was some illegal action and TRUE when everything was smoothly.
    */

    virtual bool Render_Plot_To_Buffer(std::vector<char>& Character) = 0;

    /**
     * This method renders the legend to plot into vector Legend_Buffer
     */
    void Render_Legend_To_Buffer();

    /**
     * It finds the maximum value of Raw_data vector.
     *
     * @returns double The maximum value.
     */
    double Find_Max() ;
    /**
        * This method is rendering to the output file based on what plot type we have and data in Plot_Buffer.
        *
        * @return TRUE when OK. FALSE when there was problem with the file.
        * @param[in] Name is the name of the output file.
        */
    virtual bool Output_Plot_to_File (const std::string& Name ) = 0;
   /**
    * This method is used to return Value for character. It is only used by unique combined case.
    *   @return double
    */
    double ReturnValueForChar() const;

    /**
     * This method is used to set Value for character. It is only used by unique combined case.
     *@param[in] Value The value that is to be set.
     */
    void SetValueForChar(const double Value);
    /**
    * This method is used to return value of negativity. It is only used by unique combined case.
    *   @return bool
    */
    bool ReturnNegativity() const;
    /**
     * This method is used to set flat for negativity. It is only used by unique combined case.
     *@param[in] flag The bool vale that is to be set.
     */
    void SetNegativity(bool flag);


protected:
    /**
     * Maximum height of plot possible.
     *
     * Mostly plots are exactly same size but sometimes they can be 1 lower because of symetric reasons.
     */
    size_t HeightConstant = 21;
    /**
     * Number of X axis elements in future plot.
     */
    size_t X_Axis_Element_Num = 0;
    /**
     * This is value which was calculated in Compute and it repesents how much value does one character represent.
     */
    double ValueForCharacter = 0;
    /**
     * Flag which represents if there will be negative side of the plot.
     *
     * TRUE if yes.
     * FALSE if there is no negative side.
     * default is set to FALSE.
     */
    bool Negativity = false;

    /**
     * Vector of strings. It represent the Legend buffer.
     * It is filled in Render_Legend_to_Buffer and used by Output_Plot_to_File.
     */
    std::vector<std::string> Legend_Buffer;
};


#endif //SEMESTRALKA_HORIZONTAL_PLOT_H
