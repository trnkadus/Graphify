//
// Created by sirin on 14.06.16.
//

#ifndef SEMESTRALKA_HISTOGRAM_PLOT_H
#define SEMESTRALKA_HISTOGRAM_PLOT_H


#include "Horizontal_Plot.h"
#include "Bar_Plot.h"


/**
 * This class represents histogram plot
 * Its methods are exactly altered to the needs of histogram plot.
 */
class Histogram_Plot : public Horizontal_Plot {

public:
/**
     * Default constructor.
     */
    Histogram_Plot() ;
    /**
     * Default destructor.
     */
    virtual ~Histogram_Plot();
    /**
      * This method does all rendering.
      *
      * @param[in] Character there is vector of characters to be used for filling of plot.
      * @return It returns FALSE if there was some illegal action and TRUE when everything was smoothly.
      */
    virtual bool Render_Plot_To_Buffer(vector<char>& Character );
   /**
    * This method sets the bar width of histogram bars.
    *
    * @param[in] Width the size of bar width.
    * @return FALSE if size is lower then 1 otherwise TRUE.
    *
    */
    bool Set_Bar_Width(const size_t Width = 3);
    /**
        * This method is rendering to the output file based on what plot type we have and data in Plot_Buffer.
        *
        * @return TRUE when OK. FALSE when there was problem with the file.
        * @param[in] Name is the name of the output file.
        */
    virtual  bool Output_Plot_to_File( const string& Name = "Output");
    /**
     * This method is rendering to output file combining given plots.
     *
     * @return TRUE when OK. FALSE when there was problem with the file.
     * @param[in] Name is the name of the output file.
     * @param[in] OtherPlot It is reference to the second plot used to make combined rendering
     */
    bool Combined_Output_Plot_to_File(const string& Name , Histogram_Plot & OtherPlot) ;
/**
 * This method calculates the lenght of x axis.
 */
    inline void CalcLenght(){
        Plot_Lenght = X_Axis_Element_Num*Bar_Width +1;
    }
    /**
    * This method returns Plot_Lenght. It is only used by unique combined rendering.
    */
    size_t GetLenght() const ;
private:
    /**
     * This method renders the Y/X axis into Plot_Buffer
     */
    void Render_Matrix_to_Buffer(size_t Height);

     /**
     * It is the plot lenght or the lenght of X axis.
     */
     size_t Plot_Lenght;

    /**
     * Its size of each bar in Histogram plot.
     */
    size_t Bar_Width = 3;
};


#endif //SEMESTRALKA_HISTOGRAM_PLOT_H
