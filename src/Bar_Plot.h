//
// Created by sirin on 27.05.16.
//



#ifndef SEMESTRALKA_BAR_PLOT_H
#define SEMESTRALKA_BAR_PLOT_H

#include <iostream>
#include <string>
#include "Horizontal_Plot.h"

using namespace std;


/**
     * This class represents bar plot
     * Its methods are exactly altered to the needs of bar plot.
     *
     */
class Bar_Plot : public Horizontal_Plot {
public:
    /**
     * Default constructor
     */
    Bar_Plot() ;
    /**
     * Default destructor.
     */
    virtual ~Bar_Plot();
    /**
    * This method does all rendering.
    *
    * @param[in] Character there is vector of characters to be used for filling of plot.
    * @return It returns FALSE if there was some illegal action and TRUE when everything was smoothly.
    */
    virtual bool Render_Plot_To_Buffer(vector<char>& Character);
    /**
        * This method is rendering to the output file based on what plot type we have and data in Plot_Buffer.
        *
        * @return TRUE when OK. FALSE when there was problem with the file.
        * @param[in] Name is the name of the output file.
        */
    virtual bool Output_Plot_to_File(const string& Name = "Output" );

/**
 * This method calculates the lenght of x axis.
 */
    inline void CalcLenght(){

        Plot_Lenght = X_Axis_Element_Num*3  ; //Number of elements *2 (its better thicker) + same room for separation, minus one that is extra.

    }



private:



    /**
     * This method renders the Y/X axis into Plot_Buffer
     */
    void Render_Matrix_to_Buffer(const size_t Height);
    /**
     * It is the plot lenght or the lenght of X axis.
     */
    size_t Plot_Lenght;



};

#endif //SEMESTRALKA_BAR_PLOT_H
