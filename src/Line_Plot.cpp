//
// Created by sirin on 04.06.16.
//
#include <iostream>
#include <cmath>
#include "Line_Plot.h"
#include <fstream>

using namespace std;


Line_Plot::Line_Plot() { }

Line_Plot::~Line_Plot() { }

size_t Line_Plot::GetLenght() const {
    return Plot_Lenght;
}

void Line_Plot::Render_Matrix_to_Buffer(const size_t Height) {

    CalcLenght();


    Plot_Buffer.resize(Height);
    for (size_t k = 0; k < Height; ++k) {
        Plot_Buffer[k].resize(Plot_Lenght);
    }
    for (size_t k = 0; k < Height; ++k) {
        for (size_t i = 0; i < Plot_Lenght; ++i) {
            Plot_Buffer[k][i] = ' ';
        }
    }


    if (Negativity) {

        for (size_t i = 0; i < Height; ++i) {
            if (i == Height / 2) {
                Plot_Buffer[i][0] = '+';
                for (size_t j = 1; j < Plot_Lenght; ++j) {
                    Plot_Buffer[i][j] = '.';
                }
                continue;
            }
            Plot_Buffer[i][0] = '|';
        }


    } else {
        for (size_t i = 0; i < Height; ++i) {
            if (i == Height - 1) {
                Plot_Buffer[i][0] = '+';
                for (size_t j = 1; j < Plot_Lenght; ++j) {
                    Plot_Buffer[i][j] = '.';
                }
                break;
            }
            Plot_Buffer[i][0] = '|';
        }
    }
}

bool Line_Plot::Render_Plot_To_Buffer(vector<char>& Character) {

    //if we have HeightConstant as even number since we cound X-axis in height too we have not symmetric halves of Graph
    //so we have to agjust height.
    //This only occure when we have also Negative half so flag NEGATIVITY has to be true

    if(HeightConstant < 1){return false;}

    if (Negativity && !(HeightConstant % 2)) {

        size_t ReducedHeight = HeightConstant -1;
        Render_Matrix_to_Buffer(ReducedHeight);

        for (size_t i = 0; i < ReducedHeight/2; ++i) {
            for (size_t j = 1, k = 0; j < Plot_Lenght && k < X_Axis_Element_Num; j++, k++) {

                if ((size_t) ((Raw_Data[k].second / ValueForCharacter) + 0.50001) == (ReducedHeight/2 - i)) {

                    Plot_Buffer[i][j] = Character[0];

                }
            }
        }
        for (size_t i = (ReducedHeight/2) + 1; i < ReducedHeight; ++i) {
            for (size_t j = 1, k = 0; j < Plot_Lenght && k < X_Axis_Element_Num; j++, k++) {

                if (Raw_Data[k].second > 0) {
                    continue;
                }

                if ( (size_t) abs(((Raw_Data[k].second / ValueForCharacter) - 0.50001)) == ( i - (ReducedHeight/2))) {

                    Plot_Buffer[i][j] = Character[0];

                }
            }
        }



        return 0;
    } else{
        Render_Matrix_to_Buffer(HeightConstant);
    }

        //RENDER THE DATA;
    if(Negativity){

        for (size_t i = 0; i < HeightConstant/2; ++i) {
            for (size_t j = 1, k = 0; j < Plot_Lenght && k < X_Axis_Element_Num; j++, k++) {

                if ((size_t) ((Raw_Data[k].second / ValueForCharacter) + 0.50001) == (HeightConstant/2 - i)) {

                    Plot_Buffer[i][j] = Character[0];

                }
            }
        }
        for (size_t i = (HeightConstant/2) + 1; i < HeightConstant; ++i) {
            for (size_t j = 1, k = 0; j < Plot_Lenght && k < X_Axis_Element_Num; j++, k++) {

                if (Raw_Data[k].second > 0) {
                    continue;
                }

                if ( (size_t) abs(((Raw_Data[k].second / ValueForCharacter) - 0.50001)) == ( i - (HeightConstant/2))) {

                    Plot_Buffer[i][j] = Character[0];

                }
            }
        }





    }else {

        for (size_t i = 0; i < HeightConstant - 1 ; ++i) {
            for (size_t j = 1, k = 0; j < Plot_Lenght && k < X_Axis_Element_Num; j++, k++) {


                if ((size_t) ((Raw_Data[k].second / ValueForCharacter) + 0.50001) == ((HeightConstant -1) - i)) {

                    Plot_Buffer[i][j] = Character[0];

                }
            }
        }
    }

    return true;
}

bool Line_Plot::Output_Plot_to_File(const string &Name) {

    Render_Legend_To_Buffer();


    ofstream OutFile;

    OutFile.open(Name, ios::out | ios::app);
    if(!OutFile.is_open())return false;

    if (Negativity) {

        if (HeightConstant % 2 == 0) { HeightConstant = HeightConstant - 1; };

        for (size_t i = 0; i < HeightConstant; ++i) {

            if (i < HeightConstant / 2) {
                OutFile << right << setprecision(4) << setw(6) << (((HeightConstant - 1) / 2) - i) * ValueForCharacter;
            }
            else {
                OutFile << right << setprecision(4) << setw(6) <<
                (i - ((HeightConstant - 1) / 2)) * ValueForCharacter * -1;
            }
            for (size_t j = 0; j < Plot_Lenght; ++j) {
                OutFile << Plot_Buffer[i][j] << ' ';
            }
            if (i < Legend_Buffer.size()) {
                OutFile << '\t' << Legend_Buffer[i] << endl;
            } else { OutFile << endl; }

        }
        OutFile.close();
        if(!OutFile.good())return false;
        return true;
    }

    for (size_t i = 0; i < HeightConstant; ++i) {

        OutFile << right << setprecision(4) << setw(6) << ((HeightConstant - 1) - i) * ValueForCharacter;

        for (size_t j = 0; j < Plot_Lenght; ++j) {
            OutFile << Plot_Buffer[i][j] << ' ';
        }
        if (i < Legend_Buffer.size()) {
            OutFile << '\t' << Legend_Buffer[i] << endl;
        } else { OutFile << endl; }
    }

    OutFile.close();
    if( !OutFile.good() )return false;
    return true;
}

bool Line_Plot::Combined_Output_Plot_to_File(const string& Name, Line_Plot & OtherPlot) {

    Render_Legend_To_Buffer();
   OtherPlot.Render_Legend_To_Buffer();


    ofstream OutFile;

    OutFile.open(Name, ios::out | ios::app);
    if(!OutFile.is_open())return false;

    if (Negativity) {

        if (HeightConstant % 2 == 0) { HeightConstant = HeightConstant - 1; };

        for (size_t i = 0; i < HeightConstant; ++i) {

            if (i < HeightConstant / 2) {
                OutFile << right << setprecision(4) << setw(6) << (((HeightConstant - 1) / 2) - i) * ValueForCharacter;
            }
            else {
                OutFile << right << setprecision(4) << setw(6) <<
                (i - ((HeightConstant - 1) / 2)) * ValueForCharacter * -1;
            }
            for (size_t j = 0; j < Plot_Lenght; ++j) {

                if(j < OtherPlot.Plot_Lenght){

                    if(OtherPlot.Plot_Buffer[i][j] != ' '){

                        OutFile << OtherPlot.Plot_Buffer[i][j] << ' ';

                    }else{
                        OutFile << Plot_Buffer[i][j] << ' ';
                    }
                }
                else{
                    OutFile << Plot_Buffer[i][j] << ' ';
                }
            }

            if (i < Legend_Buffer.size()) {
                OutFile << '\t' << Legend_Buffer[i] ;
                if( i < OtherPlot.Legend_Buffer.size()){ OutFile <<  "|||||  " << OtherPlot.Legend_Buffer[i] << endl;}
            } else { OutFile << endl; }

        }
        OutFile.close();
        if(!OutFile.good())return false;
        return true;
    }

    for (size_t i = 0; i < HeightConstant; ++i) {

        OutFile << right << setprecision(4) << setw(6) << ((HeightConstant - 1) - i) * ValueForCharacter;

        for (size_t j = 0; j < Plot_Lenght; ++j) {

            if(j < OtherPlot.Plot_Lenght){

                if(OtherPlot.Plot_Buffer[i][j] != ' '){


                    OutFile << OtherPlot.Plot_Buffer[i][j] << ' ';


                }else{
                    OutFile << Plot_Buffer[i][j] << ' ';
                }
            }
            else{
            OutFile << Plot_Buffer[i][j] << ' ';
            }
        }
        if (i < Legend_Buffer.size()) {
            OutFile << '\t' << Legend_Buffer[i] ;
            if( i < OtherPlot.Legend_Buffer.size()){ OutFile <<  "|||||  " <<  OtherPlot.Legend_Buffer[i] << endl;}
        } else { OutFile << endl; }
    }

    OutFile.close();
    if( !OutFile.good() )return false;
    return true;
}

















