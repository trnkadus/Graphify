//
// Created by sirin on 27.05.16.
//

#include "Graph.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdexcept>

using namespace std;

Plot::Plot() { }

Plot::~Plot() {}

bool Plot::Get_Data( const char* Input ){

  string line ;
    ifstream inputFile(Input, ios::in );
    pair<string, double> TobeAdded;
    if( inputFile.is_open() ){

        while ( getline ( inputFile , line ) )
        {
            string AddMe;
            stringstream PartLine(line);
            getline(PartLine, AddMe, ',');

            TobeAdded.first = AddMe;
            getline(PartLine, AddMe );


            try {
                TobeAdded.second = stod(AddMe) ;
            }
            catch (invalid_argument) {
                return false;
            }


            Raw_Data.push_back(TobeAdded);


        }
    }
    else{return false;}

    inputFile.close();
   // if(!inputFile.good())return false;

    return true;
}



