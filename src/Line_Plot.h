//
// Created by sirin on 04.06.16.
//

#ifndef SEMESTRALKA_LINE_PLOT_H
#define SEMESTRALKA_LINE_PLOT_H

#include "Horizontal_Plot.h"

#include <vector>


/**
* This class as well as Bar Plot specifies exactly every need of line plot.
*
* It does render the plot in the way of line plot.
*/
class Line_Plot : public Horizontal_Plot{

public:
    /**
     * Default constructor.
     */
    Line_Plot();
    /**
     * Default destructor.
     */
    virtual ~Line_Plot();
    /**
     * This method does all rendering.
     *
     * @param[in] Character there is vector of characters to be used for filling of plot.
     * @return It returns FALSE if there was some illegal action and TRUE when everything was smoothly.
     */
    virtual bool Render_Plot_To_Buffer(std::vector<char>& Character);
    /**
        * This method is rendering to the output file based on what plot type we have and data in Plot_Buffer.
        *
        * @return TRUE when OK. FALSE when there was problem with the file.
        * @param[in] Name is the name of the output file.
        */
    virtual bool Output_Plot_to_File(const std::string& Name = "Output" ) ;
    /**
       * This method is rendering to output file combining given plots.
       *
       * @return TRUE when OK. FALSE when there was problem with the file.
       * @param[in] Name is the name of the output file.
       * @param[in] OtherPlot It is reference to the second plot used to make combined rendering
       */
    bool Combined_Output_Plot_to_File(const std::string& Name , Line_Plot & OtherPlot) ;
/**
 * This method calculates the lenght of x axis.
 */
    inline void CalcLenght(){
        Plot_Lenght = X_Axis_Element_Num + 1  ;
    }
    /**
     * This method returns Plot_Lenght. It is only used by unique combined rendering.
     */
    size_t GetLenght() const ;

private:
    /**
     * It is the plot lenght or the lenght of X axis.
     */
    size_t Plot_Lenght;
    /**
     * This method renders the Y/X axis into Plot_Buffer
     */
    void Render_Matrix_to_Buffer(const size_t Height);

};


#endif //SEMESTRALKA_LINE_PLOT_H
