//
// Created by sirin on 13.06.16.
//

#include "Circual_Plot.h"
#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

#define PI 3.14159265

Circual_Plot::Circual_Plot() {

}

Circual_Plot::~Circual_Plot() { }


bool Circual_Plot::Compute(const size_t size) {

    if (size % 2 != 1) return false;
    Radius = size;

    NumberOfElements = Raw_Data.size();
    double SUM = 0;
    for (size_t i = 0; i < NumberOfElements; ++i) {

        if (Raw_Data[i].second < 0)return false;
        SUM += Raw_Data[i].second;
    }

    double Offset = 0;
    for (size_t j = 0; j < NumberOfElements; ++j) {
        //Computing Values to vector PiePiece of structures OnePieceOfPie to easily render it into pie;
        double Degree = Offset + Raw_Data[j].second / SUM * 360;
        OnePieceOfPie Insert_this(Raw_Data[j].first, Raw_Data[j].second, Raw_Data[j].second / SUM * 100, Degree);
        PiePiece.push_back(Insert_this);
        Offset = Degree;
    }


    return true;
}

char Circual_Plot::ReturnCharacter(double Angle) {

    if (Angle < 0) { Angle = 360 - fabs(Angle); }
    // count for char
    for (auto IT = PiePiece.begin(); IT != PiePiece.end(); ++IT) {

        if (Angle > IT->S_Degree) { continue; } else {

            return List_Char[(IT - PiePiece.begin()) % List_Char.size()];
        }
    }
    return '0';
}

bool Circual_Plot::Render_Plot_To_Buffer(vector<char>& Character) {

    if(Character.size() > 1){
    List_Char = Character;}

    Plot_Buffer.resize(Radius);
    for (size_t k = 0; k < Radius; ++k) {
        Plot_Buffer[k].resize(Radius);
    }
    for (size_t k = 0; k < Radius; ++k) {
        for (size_t i = 0; i < Radius; ++i) {
            Plot_Buffer[k][i] = ' ';
        }
    }
    double LenghtFromMiddle;
    double HalfRadius = Radius / 2;
    for (size_t i = 0; i < Radius; ++i) {
        for (size_t j = 0; j < Radius; ++j) {

            LenghtFromMiddle = (HalfRadius - j) * (HalfRadius - j) + (HalfRadius - i) * (HalfRadius - i);
            if (LenghtFromMiddle < HalfRadius * HalfRadius) {

                if (LenghtFromMiddle == 0) {
                    Plot_Buffer[i][j] = '0';
                    continue;
                }
                Plot_Buffer[i][j] = ReturnCharacter(atan2(HalfRadius - j, HalfRadius - i) * 180 / PI);

            }
        }
    }


    return true;
}


bool Circual_Plot::Output_Plot_to_File(const string& Name) {

    ofstream OutFile;

    OutFile.open(Name, ios::out | ios::app);
    if (!OutFile.is_open())return false;

    for (size_t i = 0; i < Radius; ++i) {


        for (size_t j = 0; j < Radius; ++j) {
            OutFile << Plot_Buffer[i][j] << ' ';
        }
        if (i < PiePiece.size()) {


            OutFile <<  '\t'  << setw(8) << PiePiece[i].S_Name << "  ...  " << PiePiece[i].S_Value << "  ...  " << setprecision( 4 )<<setw(5)<<
            PiePiece[i].S_Percentage <<
            '%' << "  ...  " << List_Char[ i % List_Char.size()] << endl;

        } else { OutFile << endl; }

    }

    OutFile.close();
    if (!OutFile.good())return false;
    return true;
}
