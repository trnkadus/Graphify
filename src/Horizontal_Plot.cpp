//
// Created by sirin on 04.06.16.
//

#include "Horizontal_Plot.h"
#include <cmath>
#include <sstream>
using namespace std;


Horizontal_Plot::~Horizontal_Plot() {}

Horizontal_Plot::Horizontal_Plot() {

    X_Axis_Element_Num =  Raw_Data.size();

}

bool Horizontal_Plot::ReturnNegativity() const {
    return Negativity;
}

void Horizontal_Plot::SetNegativity(bool flag) {

    Negativity = flag;
}

void Horizontal_Plot::SetValueForChar(const double Value) {

    ValueForCharacter = Value;
}

double Horizontal_Plot::ReturnValueForChar() const {

    return ValueForCharacter;
}

double Horizontal_Plot::Find_Max()  {
   double max= 0;


    for (auto i = Raw_Data.begin(); i != Raw_Data.end() ; ++i) {

        if(fabs(i->second) > max) {
            max = fabs(i->second);
        }
        if(i->second < 0) Negativity = true;


    }

return max;
}

bool Horizontal_Plot::Compute(const size_t Height){

    if(Height <= 1){return false;}

    HeightConstant = Height;

    double MaxValue = Find_Max();

    if(MaxValue == 0){return false;}

    if(Negativity && !(HeightConstant%2)){

        ValueForCharacter = MaxValue / ((HeightConstant-2) /2);
    }
    else if (Negativity) {
        ValueForCharacter = MaxValue / (HeightConstant /2) ;
    }
    else {
        ValueForCharacter = MaxValue / (HeightConstant-1) ;
    }
    X_Axis_Element_Num = Raw_Data.size();
    if(X_Axis_Element_Num == 0) return false;



    return true;
}

void Horizontal_Plot::Render_Legend_To_Buffer() {

    stringstream TMP_SS ;

    for (size_t i = 0; i < X_Axis_Element_Num; ++i) {

        TMP_SS.str( "" )  ;
        TMP_SS << setw( 3 )<<i+1 << ".  " << setw(10 ) << left <<Raw_Data[i].first << " ... " << setprecision(3) << setw( 5 ) << right <<Raw_Data[i].second << ' ';

        string Insert_This;
        Insert_This = TMP_SS.str();
        Legend_Buffer.push_back(Insert_This);

    }
}



