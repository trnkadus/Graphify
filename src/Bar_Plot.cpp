//
// Created by sirin on 27.05.16.
//


#include "Bar_Plot.h"
#include <cmath>
#include <fstream>

using namespace std;


Bar_Plot::~Bar_Plot() { }

Bar_Plot::Bar_Plot() { }

void Bar_Plot::Render_Matrix_to_Buffer( const size_t Height) {

    CalcLenght();


    Plot_Buffer.resize(Height);
    for (size_t k = 0; k < Height; ++k) {
        Plot_Buffer[k].resize(Plot_Lenght);
    }
    for (size_t k = 0; k < Height; ++k) {
        for (size_t i = 0; i < Plot_Lenght; ++i) {
            Plot_Buffer[k][i] = ' ';
        }
    }


    if (Negativity) {

        for (size_t i = 0; i < Height; ++i) {
            if (i == Height / 2) {
                Plot_Buffer[i][0] = '+';
                for (size_t j = 1; j < Plot_Lenght; ++j) {
                    Plot_Buffer[i][j] = '.';
                }
                continue;
            }
            Plot_Buffer[i][0] = '|';
        }


    } else {
        for (size_t i = 0; i < Height; ++i) {
            if (i == Height - 1) {
                Plot_Buffer[i][0] = '+';
                for (size_t j = 1; j < Plot_Lenght; ++j) {
                    Plot_Buffer[i][j] = '.';
                }
                break;
            }
            Plot_Buffer[i][0] = '|';
        }
    }
}

bool Bar_Plot::Render_Plot_To_Buffer(vector<char>& Character) {

    //if we have HeightConstant as even number since we cound X-axis in height too we have not symmetric halfs of Graph
    //so we have to agjust height.
    //This only occure when we have also Negative half so flag NEGATIVITY has to be true

    if(HeightConstant < 1){return false;}

    if (Negativity && !(HeightConstant % 2)) {
        size_t ReducedHeight = HeightConstant - 1;
        Render_Matrix_to_Buffer(ReducedHeight);

        for (size_t i = 0; i < ReducedHeight / 2; ++i) {
            for (size_t j = 1, k = 0; j < Plot_Lenght && k < X_Axis_Element_Num; j++, k++) {

                if (((Raw_Data[k].second / ValueForCharacter) + 0.50001) > (ReducedHeight / 2 - i)) {

                    Plot_Buffer[i][j++] = Character[0];
                    Plot_Buffer[i][j] = Character[0];

                    j++;
                } else {
                    j++;
                    j++;
                }
            }
        }
        for (size_t i = (ReducedHeight / 2) + 1; i < ReducedHeight; ++i) {
            for (size_t j = 1, k = 0; j < Plot_Lenght && k < X_Axis_Element_Num; j++, k++) {

                if (Raw_Data[k].second > 0) {
                    j++;
                    j++;
                    continue;
                }
                if (abs(((Raw_Data[k].second / ValueForCharacter)) - 0.50001) > (i - ReducedHeight / 2)) {

                    Plot_Buffer[i][j++] = Character[0];
                    Plot_Buffer[i][j] = Character[0];
                    j++;
                } else {
                    j++;
                    j++;
                }
            }
        }


        return 0;
    } else {
        Render_Matrix_to_Buffer(HeightConstant);
        //Since nothing special is needed here we continue method as intended.
    }

    //RENDER THE DATA;

    if (Negativity) {
        for (size_t i = 0; i < HeightConstant / 2; ++i) {
            for (size_t j = 1, k = 0; j < Plot_Lenght && k < X_Axis_Element_Num; j++, k++) {

                if (((Raw_Data[k].second / ValueForCharacter) + 0.50001) > (HeightConstant / 2 - i)) {

                    Plot_Buffer[i][j++] = Character[0];
                    Plot_Buffer[i][j] = Character[0];

                    j++;
                } else {
                    j++;
                    j++;
                }
            }
        }
        for (size_t i = (HeightConstant / 2) + 1; i < HeightConstant; ++i) {
            for (size_t j = 1, k = 0; j < Plot_Lenght && k < X_Axis_Element_Num; j++, k++) {

                if (Raw_Data[k].second > 0) {
                    j++;
                    j++;
                    continue;
                }
                if (abs(((Raw_Data[k].second / ValueForCharacter)) - 0.50001) > (i - HeightConstant / 2)) {

                    Plot_Buffer[i][j++] = Character[0];
                    Plot_Buffer[i][j] = Character[0];
                    j++;
                } else {
                    j++;
                    j++;
                }
            }
        }


    }


    else {
        for (size_t i = 0; i < HeightConstant - 1; ++i) {
            for (
                    size_t j = 1, k = 0; j < Plot_Lenght && k < X_Axis_Element_Num; j++, k++) {


                if (((Raw_Data[k].second / ValueForCharacter) + 0.50001) > ((HeightConstant - 1) - i)) {

                    Plot_Buffer[i][j++] = Character[0];
                    Plot_Buffer[i][j] = Character[0];

                    j++;
                } else {
                    j++;
                    j++;
                }
            }
        }
    }



    return true;
}


bool Bar_Plot::Output_Plot_to_File(const string & Name ) {

    /////////////////////////////////////////////////////////////////////////////

    Render_Legend_To_Buffer();

    /////////////////////////////////////////////////////////////////////////////

    ofstream OutFile;

    OutFile.open(Name, ios::out | ios::app);
    if(!OutFile.is_open())return false;

    if (Negativity) {

        if (HeightConstant % 2 == 0){ HeightConstant = HeightConstant - 1; };

        for (size_t i = 0; i < HeightConstant; ++i) {

            if (i < HeightConstant / 2) {
                OutFile << right << setprecision(4) << setw(6) << (((HeightConstant - 1) / 2) - i) * ValueForCharacter;
            }
            else {
                OutFile << right << setprecision(4) << setw(6) <<
                (i - ((HeightConstant - 1) / 2)) * ValueForCharacter * -1;
            }
            for (size_t j = 0; j < Plot_Lenght; ++j) {
                OutFile << Plot_Buffer[i][j] << ' ';
            }
            if (i < Legend_Buffer.size()) {
                OutFile << '\t' << Legend_Buffer[i] << endl;
            } else { OutFile << endl; }

        }
        OutFile.close();
        if(!OutFile.good())return false;
        return true;
    }

    for (size_t i = 0; i < HeightConstant; ++i) {

        OutFile << right << setprecision(4) << setw(6) << ((HeightConstant - 1) - i) * ValueForCharacter;

        for (size_t j = 0; j < Plot_Lenght; ++j) {
            OutFile << Plot_Buffer[i][j] << ' ';
        }
        if (i < Legend_Buffer.size()) {
            OutFile << '\t' << Legend_Buffer[i] << endl;
        } else { OutFile << endl; }
    }

    OutFile.close();
    if( !OutFile.good() )return false;
    return true;
}

