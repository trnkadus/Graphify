CXX=g++
LD=g++
CXXFLAGS=-std=c++11 -Wall -pedantic -Wno-long-long -O0 -ggdb 
OBJS=obj/main.o obj/Graph.o obj/Horizontal_Plot.o obj/Bar_Plot.o obj/Line_Plot.o obj/Histogram_Plot.o obj/Circual_Plot.o

all: compile 

compile: init trnkadus doc

init:
	mkdir -p obj
run:
	./trnkadus
trnkadus: $(OBJS)	
	$(LD) -o trnkadus $^
doc:
	mkdir -p doc
	doxygen doxyfile

clean:
	rm -rf obj
	rm -rf doc
	rm -f trnkadus
	rm -f Output

obj/main.o: src/main.cpp src/Graph.h src/Bar_Plot.h src/Horizontal_Plot.h src/Line_Plot.h src/Histogram_Plot.h src/Circual_Plot.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
obj/Graph.o: src/Graph.cpp src/Graph.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
obj/Horizontal_Plot.o: src/Horizontal_Plot.cpp src/Horizontal_Plot.h src/Graph.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
obj/Circual_Plot.o: src/Circual_Plot.cpp src/Circual_Plot.h src/Graph.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
obj/Bar_Plot.o: src/Bar_Plot.cpp src/Bar_Plot.h src/Horizontal_Plot.h src/Graph.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
obj/Line_Plot.o: src/Line_Plot.cpp src/Line_Plot.h src/Horizontal_Plot.h src/Graph.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
obj/Histogram_Plot.o: src/Histogram_Plot.cpp src/Histogram_Plot.h src/Horizontal_Plot.h src/Graph.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<


