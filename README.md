This program is meant to draw in ASCII characters a plot based on input file
 
Program is written in C++11. For compilation please use make all.
 
make doc will generate doxygen documentation in /doc folder.
 
 
All example input files are in examples/ folder.

Usage: ./trnkadus ./examples/config ./examples/ "any input file from examples"


Config file format:
 
int,int, "any number of individual char separated by ,"

 
 First int means height of desired graph.
 Secound int is only used by Histrogram Plot and it is used for setting how many charaters wide should one column be.
  Other characters represents the filling of graph. For pie chart for example.
 
 Example usage:
 
 ./trnkadus ./examples/config ./examples/sinus
 ./trnkadus ./examples/config ./examples/cosinus
 
 ./trnkadus ./examples/wideconfig ./examples/sinus 
        and choose histogram.
 
 ./trnkadus ./examples/config ./examples/randominput1 
        and choose pie chart.
 